# absence-o-linette Mobile


## documentation technique

### prerequis

pour que les commande suivantes fonctionnent, vous devez vous assurez que les programmes ci-dessous soient installé sur votre OS.

 * git ([doc installation](https://git-scm.com/book/fr/v1/D%C3%A9marrage-rapide-Installation-de-Git))
 * nodejs ([doc installation](https://nodejs.org/en/download/package-manager/))
 * ionic (npm install -g cordova ionic)


### installation

pour avoir les permissions d'acces au depot, veuillez contacter le responsable technique 

fabien martinez (fabien1.martinez@epitech.eu)


``` bash
git clone https://erwan_touba@bitbucket.org/erwan_touba/student_scanner.git
```
ou

``` bash
git clone git@bitbucket.org:erwan_touba/student_scanner.git
```

ensuite installez les plugins nodejs / cordova s'il y en a.
``` bash
npm install
```

### deployement

1. activer le debogage USB sur le votre smartphone / tablette.
2. connectez votre smartphone / tablette au PC via un cable USB.
3. tapez la commande :
``` bash
ionic run android
```

cela va mettre l'application sur votre smartphone / tablette et la lancer automatiquement au bout de quelques secondes (30-45).

**ca y est**, l'application est installe sur votre telephone.