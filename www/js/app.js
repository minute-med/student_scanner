(function(window){

// initialisation de la variable d'environement 'env'
var __env = {};


// Importer les variable de __env dans env.js

if(window){
  __env = Object.assign(__env, window.__env);
}

}(this));

var app = angular.module('pointeuse', ['ionic', 'ionic-timepicker', 'pointeuse.controllers', 'pointeuse.services'])

.run(function($ionicPlatform, $rootScope, $state, $http) {


  if(!$http.defaults.headers.common['Authorization'] && localStorage.getItem('access_token')) {
     $http.defaults.headers.common['Authorization'] = 'basic ' + window.btoa(localStorage.getItem('access_token'));
  }

  $rootScope.$on("$locationChangeStart",function(event, next, current){
    
    if (!localStorage.getItem('access_token')) {
      $state.go('login');
    }

  });

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

  $httpProvider.interceptors.push('httpRequestInterceptor');

  $stateProvider

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.scan', {
    url: '/scan/:nextState',
    views: {
      'menuContent': {
        templateUrl: 'templates/scan.html',
        controller: 'ScanCtrl'
      }
    }
  })

  .state('app.help', {
    url: '/help',
    views: {
      'menuContent': {
        templateUrl: 'templates/help.html'
      }
    }
  })

  .state('app.check', {
    url: '/check/:codebar/:referer',
    views: {
      'menuContent': {
        templateUrl: 'templates/check.html',
        controller: 'CheckCtrl'
      }
    }
  })

  .state('app.manual_check', {
    url: '/manual_check',
    views: {
      'menuContent': {
        templateUrl: 'templates/manual_check.html',
        controller: 'manualCheckCtrl'
      }
    }
  })

  .state('app.assign', {
    url: '/assignation/:codebar',
    views: {
      'menuContent': {
        templateUrl: 'templates/assignation.html',
        controller: 'AssignCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/help');
});

// Enregistre la variable comme une constante (ducoup disponible en injection de dependance partout) 
app.constant('__env', __env);