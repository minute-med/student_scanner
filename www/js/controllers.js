angular.module('pointeuse.controllers', [])

.controller('AppCtrl', function($scope, $rootScope) {

  $scope.external_link = function(url){
    cordova.InAppBrowser.open(url, "_system", "location=true");
    return false;
  }

})

.controller('loginCtrl', function($state, $scope,$http, User){

  $scope.user = {};
  $scope.errors = {};

  $scope.login = function(){

    User.auth($scope.user, function(response){

      var token = response.data.access_token;
      localStorage.setItem('access_token', token);
      $http.defaults.headers.common['Authorization'] = 'basic ' + window.btoa(token);
      $scope.errors = {};
      $state.go('app.help');

    }, function(error){

      alert('auth error');
      console.log(error.data);
      $scope.errors = error.data;

    });


  }

})

.controller('ScanCtrl', function($scope, $state, $stateParams, BarcodeScaner){

  $scope.$on('$ionicView.enter', function(e) {

    BarcodeScaner.scan(function(result){
      $state.go($stateParams.nextState, 
        {codebar: parseInt(result.text), referer: 'app.scan'},
        {reload:true});
    });

  });
})

.controller('manualCheckCtrl', function($scope, $state, $stateParams, Students){

  $scope.students = [];
  $scope.referer = $stateParams.referer;

  $scope.toggle_multicheck = function(){ alert('toggle multicheck')}

  $scope.$on('$ionicView.enter', function(e) {

    Students.registred(function(results){

      console.log('results');
      console.debug(results);

      var students = results.data.data;
      $scope.students = students;

    }, function(error){

      console.log(error);
      alert('erreur');

    });

  });

  $scope.select = function(student, referer){

    $state.go('app.check', {codebar: student.codebar, referer: 'app.manual_check'});
  }

})

.controller('AssignCtrl', function($state, $scope, $stateParams, $ionicPopup, Students) {

  $scope.students = [];

  $scope.assign = function(student_id){

    postdata = JSON.stringify({
      student_id: student_id,
      codebar: $stateParams.codebar
    });

    Students.assign(postdata, function(result){

      console.log(result.data.data);

      var student = result.data.data;

      var confirmPopup = $ionicPopup.confirm({
        title: 'Continuer',
        template: student.firstname + " " + student.lastname + " porte le code " + student.codebar + "\nVoulez vous continuer ?",
        cancelText: 'Oui', // String (default: 'Cancel'). The text of the Cancel button.
        okText: 'Non'
      });

      confirmPopup.then(function(res) {
       if(res) {
        $state.go('app.help');
       } else {
        $state.go('app.scan', {nextState: 'app.assign'}, {reload:true});
      }
    });

    });

  }

  $scope.$on('$ionicView.enter', function(e) {

    Students.unregistred(function(results){

      var students = results.data.data;
      $scope.students = students;

    });

  });

})

.controller('CheckCtrl', function( 
  $scope,
  $state, 
  $stateParams, 
  Students, 
  ionicTimePicker){

  $scope.student = {};
  $scope.backToHome = {isChecked: false};
  $scope.time = undefined;


  var ipObj1 = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {

          var selectedTime = new Date(val * 1000);
          var selectedDateTime = new Date();
          selectedDateTime.setUTCHours(selectedTime.getUTCHours());
          selectedDateTime.setUTCMinutes(selectedTime.getUTCMinutes());
          selectedDateTime.setUTCSeconds(0);

          $scope.time = selectedDateTime;
        }
      },
      format: 24, //Optional
      step: 1,
      setLabel: 'Valider',
      closeLabel: 'Annuler'
    };

  $scope.setTime = function () {
    ionicTimePicker.openTimePicker(ipObj1);
  }

  $scope.$on('$ionicView.enter', function(e) {

    Students.findOneWhere({codebar: $stateParams.codebar},function(response){

      console.log(response);

      var student = response.data.data;
      $scope.student = student;

    }, function(error){

      alert("pas d'etudiant enregistre avec ce codebar");
      $state.go($stateParams.referer, {nextState: 'app.check'}, {reload:true});

    });

  });

  $scope.check = function(check){

    data = {
      codebar:  $stateParams.codebar,
      check:    check,
      time:     $scope.time
    };

    Students.check(data, function(result){

      if($scope.backToHome.isChecked == true) {

        $state.go('app.help', {}, {reload:true});
      } else {
        $state.go($stateParams.referer, {nextState: 'app.check'}, {reload:true});
      } 
    });

  }

  $scope.cancel = function(){}

});