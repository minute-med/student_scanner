angular.module('app.directives', [])

.directive('studentsDisplay', function() {
  
  return {
    templateUrl: 'templates/directives/students-display.html'
  };

});