angular.module('pointeuse.services', [])


.service('User', function($http, $state, __env) {

	this.auth = function(credentials, successCallback, errorCallback){

		var url = __env.apiUrl + 'auth';
		$http.post(url, credentials).then(successCallback, errorCallback);
	}

})

/*
	Service students qui tappe dans le webservice laravel
*/
.service('Students', function($http, $state, __env){

	this.assign = function(data, successCallback, errorCallback){

		if (typeof errorCallback == 'undefined')
			errorCallback = function(response){
				console.debug(response);
		      var str_error = '';
		      var errors = response.data.error;
		      for (i in errors){
		        str_error += errors[i].join("\n");
		      }
		      alert("erreur : \n" + str_error);
		      $state.go('app.scan',{nextState: 'app.assign'});
		  }

		var url = __env.apiUrl + 'students/associate';
		$http.post(url, data).then(successCallback, errorCallback);
	}

	this.check = function(data, successCallback, errorCallback){

		var url = __env.apiUrl + 'students/check';
		$http.post(url, data).then(successCallback, errorCallback);
	}

	this.all = function(successCallback, errorCallback){

		var url = __env.apiUrl + 'students';
		$http.get(url).then(successCallback, errorCallback);
	};

	this.unregistred = function(successCallback, errorCallback){

		var url = __env.apiUrl + 'students/unregistred';
		$http.get(url).then(successCallback, errorCallback);
	};

	this.registred = function(successCallback, errorCallback){

		var url = __env.apiUrl + 'students/registred';
		$http.get(url).then(successCallback, errorCallback);
	};

	this.find = function(id, successCallback, errorCallback) {

		var url = __env.apiUrl + 'students/' + id;
		$http.get(url).then(successCallback, errorCallback);
	}

	this.findOneWhere = function(search, successCallback, errorCallback) {

		var url = __env.apiUrl + 'students/findone';
		$http.post(url, search).then(successCallback, errorCallback);
	}

	this.findWhere = function(search, successCallback, errorCallback) {

		var url = __env.apiUrl + 'students/find';
		$http.post(url, search).then(successCallback, errorCallback);
	}
})

/* Wrapping du plugin cordova barcode scanner */
.service('BarcodeScaner', function($state, $ionicPlatform){

	this.scan = function(successCallback, errorCallback){

		if (typeof errorCallback == 'undefined' || typeof errorCallback != 'function')
			var errorCallback = function(error){

		      var str_error = '';
		      for (i in error.data){
		        str_error += error.data[i].join("\n");
		      }    
		      alert("scan failed : \n" + error);
		      $state.reload();
		  }
		$ionicPlatform.ready(function(){
			cordova.plugins.barcodeScanner.scan(successCallback, errorCallback);
		});

	}

})

/* pour eviter les preflight request OPTION on change le content type */
.factory('httpRequestInterceptor', function () {
  return {
    request: function (config) {

      if(config.method == 'POST')
      	config.headers["Content-Type"] = "text/plain";
      return config;
      
    }
  };
});
